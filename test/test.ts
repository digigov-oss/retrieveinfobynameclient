import getInfo from '../src/index';
import config from './config.json'; 
import inspect from 'object-inspect';
const test = async () => {
   
   //Try compinations to check the service in full.
   const inputs = [{
    surnameA: "ΠΑΠΠΑΣ",
    nameA: "ΓΕΩΡΓ",
    fathersFirstName: "ΚΩΝ"
   },
   {
    surnameA: "ΠΑΠΑΔΟΠΟΥΛΟΣ",
    nameA: "ΚΩΝΣΤ",
    fathersFirstName: "ΓΕΩ"
   },
   {
    surnameA: "ΠΑΠΑΔΟΠΟΥΛΟΥ",
    nameA: "ΕΛΕΝΗ",
    fathersFirstName: "ΓΕΩΡΓΙΟΣ"
   },
 ]
   //loop on inputs array
    for (const input of inputs) {
    try {
        const Info = await getInfo(input, config.user, config.pass);
        console.log(inspect(Info,{depth:10,indent:"\t"})); 
    } catch (error) {
        console.log(error);
    }
    }


}

test();