import { retrieveInfoByNameInputRecord } from './index.js';
import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
/**
 * SOAP client for retrieveInfoByAFM
 * @class Soap
 * @description SOAP client for retrieveInfoByAFM
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 * @param {string} endpoint
 */
declare class Soap {
    private _wsdl;
    private _username;
    private _password;
    private _auditRecord;
    private _endpoint;
    constructor(wsdl: string, username: string, password: string, auditRecord: AuditRecord, endpoint: string);
    init(): Promise<any>;
    getInfo(input: retrieveInfoByNameInputRecord): Promise<any>;
}
export default Soap;
