import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
export declare type record = {
    afm: string;
    surname: string;
    secondSurname: string;
    firstName: string;
    fathersFirstName: string;
    mothersFirstName: string;
    birthDate: string;
    deathDate: string;
    doy: string;
    doyDescr: string;
    cardNo: string;
    cardKindDescr: string;
    firmFlagDescr: "1" | "2" | "3";
    residenceAddress: string;
    residenceNo: string;
    parResidence: string;
    parResidenceDescr: string;
    firmAddress: string;
    firmNo: string;
    parBusiness: string;
    parBusinessDescr: string;
    code: string;
    description: string;
};
export declare type retrieveInfoByNameRecord = {
    dataNameArray: {
        record: record[];
    };
};
export declare type errorRecord = {
    errorCode: string;
    errorDescr: string;
};
export declare type retrieveInfoByNameInputRecord = {
    surnameA: string;
    surnameB?: string;
    nameA: string;
    nameB?: string;
    fathersFirstName: string;
    mothersFirstName?: string;
    birthYear?: string;
    birthMonth?: string;
    birthDay?: string;
    carPlateNumber?: string;
};
export declare type retrieveInfoByNameRequest = {
    nameRecord: retrieveInfoByNameInputRecord;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord: errorRecord | null;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param input retrieveInfoByNameInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns retrieveInfoByNameRecord | errorRecord
 */
export declare const getInfo: (input: retrieveInfoByNameInputRecord, user: string, pass: string, overrides?: overrides | undefined) => Promise<any>;
export default getInfo;
